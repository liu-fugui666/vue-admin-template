
import { getToken, setToken, removeToken, setTimeStamp } from '../../utils/auth'
import { login } from '../../api/user'
import { resetRouter } from '@/router'
const state = {
  token: getToken(), // 设置token的共享状态, 初始化vuex的时候, 就先从缓存中读取
  userInfo: {} // 定义一个空对象,用来存储用户信息
}
const mutations = {
  setToken(state, token) {
    state.token = token // 将数据设置给vuex
    // 同步到本地缓存
    setToken(token)
  },
  removeToken(state) {
    state.token = null // 将vuex中的数据置空
    // 从缓存中删除
    removeToken()
  }

}
const actions = {
  // 如果获取到的数据需要存到vuex中,那么优先在actions中发请求
  // 为什么要往vuex中存的数据需要放到actions
  // 因为vuex中的数据大多都是多页面共享的,那么肯定会出现多页面要修改,
  // 如果放到actions中,每个页面都直接调用
  // 如果放到api中,每个页面虽然可以直接调用,但是需要自己修改mutations

  // 1. 调用接口
  async login(context, payload) {
    const result = await login(payload) // 拿到token
    context.commit('setToken', result) // 存token
    setTimeStamp() // 讲当前时间戳存入本地存储
  },
  // async getUserInfo(context, payload) {
  //   const result = await getUserInfo()
  //   // 获取用户详情
  //   const baseInfo = await getUserDetailById(result.userId)
  //   context.commit('setUserInfo', { ...result, ...baseInfo })
  //   return result
  // },
  logout(context) {
    // 1. 清除token
    context.commit('removeToken')
    // 2. 清除用户资料
    context.commit('removeUserInfo')
    // 3. 重置路由
    resetRouter()
    // 4. 清空vuex的routes(可选操作)
    // Vuex子模块调用子模块的actions
    // 还是通过模块名调用,多了一个参数{root: true},代表以根节点调用
    context.commit('permission/setRoutes', [], { root: true })
  }
}

export default {
  state,
  mutations,
  actions,
  // 开启命名空间
  namespaced: true
}
