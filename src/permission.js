/*
  路由守卫配置
*/

import router from '@/router'
import store from '@/store'
import NProgress from 'nprogress' // 引入一份进度条插件
import 'nprogress/nprogress.css' // 引入进度条样式

const whiteList = ['/login', '/404']
// 路由前置守卫: 在页面执行跳转的时候触发(跳转进入新的路由前)
router.beforeEach(async(to, from, next) => {
  NProgress.start() // 开启进度条
  // 判断是否有token
  if (store.getters.token) {
    // 判断是不是去登录页
    // to.path 或者 from.path 可以判断从哪儿,到哪儿去
    // next() -> 放行
    // next('/login) -> 跳转登录页
    // next(false) -> 不放行
    if (to.path === '/login') {
      next('/')
    } else {
      // 权限处理
      // if (!store.getters.userId) {
      //   const { roles } = await store.dispatch('user/getUserInfo')
      //   const routes = await store.dispatch('permission/filterRoutes', roles.menus)
      //   // 动态添加路由 - addRoutes(路由对象)
      //   router.addRoutes([...routes, { path: '*', redirect: '/404', hidden: true }])
      //   next(to.path) // 相当于多做一次跳转,原来的哈希值就不见了,需要重新跳转获取最新的哈希值
      // }
      next()
    }
  } else {
    // 判断是否为白名单
    // 1. indexOf  2. .some  3. includes
    if (whiteList.includes(to.path)) {
      next()
    } else {
      next('/login')
    }
  }
  NProgress.done() // 手动关闭,防止手动切换页面时进度条一直存在
})
// 后置路由守卫: 在页面需要执行跳转的时候触发, (跳转离开旧的路由前)
router.afterEach(() => {
  NProgress.done() // 关闭进度条
})
