// 引入axios
import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
import { getTimeStamp } from '@/utils/auth'
const Timeout = 24 * 60 * 60 * 1000 // 设置超时时间

// 创建axios实列
const service = axios.create({
  // npm run dev => /api      npm run build => /prod-api
  // 当执行 npm run dev => .evn.development => /api=> 跨域代理
  baseURL: process.env.VUE_APP_BASE_API,
  // process.env.VUE_APP_BASE_API,
  // 或者 baseURL: process.env.NODE_ENV === 'production' ? '/prod-api' : 'api'
  // 如果baseURL没有携带任何的域名协议端口号,那么默认发出的请求就是localhost
  // 发出去请求的真实地址 = 跟地址(localhost:8888/api) + 请求路径
  timeout: 12 * 1000
})
// 给实列添加请求拦截器
service.interceptors.request.use(
  config => {
    // 判断是否存在token 将config的请求头中加入token数据
    if (store.getters.token) {
      // token过期的主动处理，在发起请求的时候做时间戳判断是否过期
      if (Date.now() - getTimeStamp() > Timeout) {
        // 为true表示过期了 做登出操作
        store.dispatch('user/logout')
        router.push('/login')
        // 抛出异常, 返回失败状态, 直接进入 catch
        return Promise.reject(new Error('登录状态失效,请重新登录'))
      }
      config.headers['Authorization'] = `Bearer ${store.getters.token}`
    }
    // config是请求的配置信息 必须返回
    return config
  },
  error => {
    return Promise.reject(error)
  })

// 给实列添加响应拦截器
service.interceptors.response.use(
  // 接口调用成功
  response => {
    const { success, message, data } = response.data
    // 根据success的成功与否决定往下的操作(业务有没有成功)
    if (success) {
      return data // success 位 true 返回data信息
    } else {
      Message.error(message) // 提示错误信息
      return Promise.reject(new Error(message)) // 捕获到错误信息直接跳到 catch
    }
  },
  // 接口调用失败
  error => {
    // 不用在页面 tyr catch捕获错误后再提示了,这里统一提示
    // token 过期的被动介入， 通过token过期的错误码
    if (error.response && error.response.data && error.response.data.code === 10002) {
      // 当code为100002的时候，表示token过期了
      store.dispatch('user/logout') // 退出登录清除token和用户信息
      router.push('/login') // 跳转登录页
      Message.error('登录状态失效,请重新登录') // 给出错误信息
    }
    Message.error(error) // 给出错误信息
    return Promise.reject(error) // 返回执行错误,让当前的执行链跳出成功 直接进入 catch
  })
// 导出axios实例
export default service
